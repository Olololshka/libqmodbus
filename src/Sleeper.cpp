/*
 * Sleeper.cpp
 *
 *  Created on: 05.06.2012
 *      Author: tolyan
 */

#include "Sleeper.h"

void Sleeper::msleep(int ms)
{
    QThread::msleep(ms);
}

void Sleeper::sleep_ms(int ms)
{
    msleep(ms);
}

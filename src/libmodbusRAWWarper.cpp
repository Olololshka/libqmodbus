/*
 * libmodbusRAWWarper.cpp
 *
 *  Created on: 23.01.2012
 *      Author: shiloxyz
 */

#include <cstdlib>
#include <cstring>

#include <modbus/modbus-version.h>

#include "libmodbusRAWWarper.h"

QMap<modbus_t*, libmodbusRAWWarper*> libmodbusRAWWarper::instanceMap;

libmodbusRAWWarper::~libmodbusRAWWarper()
{
    close();
    if (ModBus)
    {
        modbus_free(ModBus);
        ModBus = NULL;
    }
}

libmodbusRAWWarper::libmodbusRAWWarper(const char *device, int baud,
                                       char parity, int data_bit, int stop_bit)
{
    buff[0] = 0;
    _pfHoockFun = NULL;
    OpenAddress = 0;
    fOpend = false;
    isUseCRC16 = 1;
    ModBus = NULL;
    DatafieldLen = 0;

    if (!fOpend)
        SetUARTMode(device, baud, parity, data_bit, stop_bit);
    instanceMap.insert(ModBus, this);
}

int libmodbusRAWWarper::connect(char address)
{
    int result;
    if (ModBus)
    {
        if (OpenAddress == 0)
        { // открытия не было
            mutex.lock(); // мьютекс защищает от подключения без закрытия
            if (modbus_connect(ModBus))
            {
                mutex.unlock();
                fOpend = 0;
                result = -2;
            }
            else
            {
                modbus_set_slave(ModBus, address);
                fOpend = 1;
                buff[0] = address;
                OpenAddress = address;
                result = 0;
            } // открытие завершено успешно
        }
        else
        { //уже открыто, но на другой адресс, мьютекс не отпускать
            modbus_set_slave(ModBus, address);
            fOpend = 1;
            buff[0] = address;
            OpenAddress = address;
            result = 0;
        }
    }
    else
    { // ModBus не создано
        result = -1;
    }
    return result;
}

void libmodbusRAWWarper::setDebug(bool isDebug)
{
    if (ModBus)
        modbus_set_debug(ModBus, isDebug);
}

unsigned char libmodbusRAWWarper::SendRAWData(unsigned char *Data,
                                              unsigned char Length)
{
    if (Data[0] == buff[0])
    {
        memcpy(&buff[1], &Data[1], Length - 1);
        return modbus_send_raw_request(ModBus, buff, Length);
    }
    else
        return 0;
}

char libmodbusRAWWarper::ReceiveRAWData(unsigned char *DataPlace)
{
    unsigned char tbf[256];
    int resived = modbus_receive_confirmation(ModBus, tbf);
    if (resived > 0)
        std::memcpy(DataPlace, tbf, resived);
    return resived;
}

void libmodbusRAWWarper::close(void)
{
    if ((ModBus) && (fOpend))
    {
        modbus_close(ModBus);
        fOpend = 0;
        buff[0] = 0;
        mutex.unlock();
        OpenAddress = 0;
    }
}

char libmodbusRAWWarper::SetUARTMode(const char *device, int baud, char parity,
                                     int data_bit, int stop_bit)
{
    if (fOpend)
    {
        modbus_close(ModBus);
        modbus_free(ModBus);
        fOpend = 0;
    }
    if ((ModBus = modbus_new_rtu(device, baud, parity, data_bit, stop_bit))
            == NULL)
        return 1;
    else
    {
#if LIBMODBUS_VERSION_HEX < 0x030101
        struct timeval timeout =
        {
            0, 175000
            // стабильно пака
        };
        modbus_set_response_timeout(ModBus, &timeout);
#else
         modbus_set_response_timeout(ModBus, 0, 175000);
#endif
#ifndef NDEBUG
        modbus_set_debug(ModBus, true);
#else
        modbus_set_error_recovery(ModBus, MODBUS_ERROR_RECOVERY_LINK);
#endif
        return 0;
    }
}

unsigned char libmodbusRAWWarper::MbDataLength_Hoock(int function,
                                                     msg_type_t msg_type,
                                                     modbus_t* modbus_instance)
{
    libmodbusRAWWarper* _this = instanceMap[modbus_instance];
    if (_this->_pfHoockFun)
    {
        if ((_this->DatafieldLen = _this->_pfHoockFun(_this->buff, 2)) > 0)
            return 1;
        else
            return 0;
    }
    else
        return 0;
}

int libmodbusRAWWarper::MbDataAfterMeta_Hoock(uint8_t *msg,
                                              int *RessivedLength,
                                              msg_type_t msg_type,
                                              modbus_t* modbus_instance)
{
    libmodbusRAWWarper* _this = instanceMap[modbus_instance];
    if (_this->_pfHoockFun)
    {
        signed char popravka = 0;
        if (msg[0] == 0x00) //ловушка пустого байта
        {
            std::memmove(msg, msg + 1, 3);
            ++popravka;
            *RessivedLength -= 1;
        }
        if (_this->DatafieldLen == 0)
            return (_this->isUseCRC16) ? (popravka + 2) : (popravka);

        if (_this->isUseCRC16)
            return _this->DatafieldLen + 2 - 1 + popravka;
        else
            return _this->DatafieldLen - 1 + popravka;
    }
    else
        return 0;
}

void libmodbusRAWWarper::SetUseCRC16(bool _isUseCRC16)
{
    if (ModBus)
    {
        isUseCRC16 = _isUseCRC16;
        modbus_set_use_CRC16(ModBus, isUseCRC16);
    }
}

void libmodbusRAWWarper::SetHoock(
        unsigned char(*pfHoockFun)(unsigned char* buff, char ResivedLength))
{
    _pfHoockFun = pfHoockFun;
    if (pfHoockFun != NULL)
    {
        modbus_set_function_hooks(ModBus, (void*)MbDataLength_Hoock,
                                  (void*)MbDataAfterMeta_Hoock);
    }
    else
    {
        modbus_set_function_hooks(ModBus, NULL, NULL);
    }
}

void libmodbusRAWWarper::flush(void)
{
    modbus_flush(ModBus);
}

modbus_t *libmodbusRAWWarper::getModbusContext(void) const
{
    return ModBus;
}

bool libmodbusRAWWarper::isOpend(void)
{
    return fOpend;
}

unsigned char libmodbusRAWWarper::getOpenedAddress(void)
{
    return (fOpend) ? (OpenAddress) : (0);
}

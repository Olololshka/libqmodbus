/**
 * @file libmodbusRAWWraper.h
 * @brief Содержит объявление класса libmodbusRAWWarper
 */

#ifndef LIBMODBUSRAWWARPER_H_
#define LIBMODBUSRAWWARPER_H_

#include <modbus/modbus.h>
#include <QMutex>
#include <QMap>

/**
 * @brief libmodbusRAWWarper Класс "врапер" для интерфейса с библиотекой libmodbus в режиме RTU в стиле C++
 *
 * Данный класс используется вместе с модифицированной библиотекой libmodbus (),
 * В которую добавлена возможность игнорирования контрольных сумм при передаче и возмодность
 * обработки нестандартных запросов черех хуки.
 */
class libmodbusRAWWarper
{
public:
    /**
     * @brief Конструктор, создающий новое соединение Modbus RTU
     * @param device Строка, содержащее имя последовательного (COM) порта ПК.
     *              Для Windows это обычно COM<x> (x - номер по порядку)
     *              Для Linux это /dev/tty<x><y> (x - тип устройства, например S иди USB, y - номер по порядку)
     * @param baud Скорость пердачи [бод] из стандартного ряда (9600, 38400, 115200 и тп.)
     * @param parity Проверка четности.
     * <table>
     *      <tr><th>'N'      <td>Без проверки четности
     *      <tr><th>'E'      <td>Проверка четности
     *      <tr><th>'O'      <td>Проверка нечетности
     * </table>
     * @param data_bit количество бит данных: от 5 до 8
     * @param stop_bit количество стоп-битов: 1 или 2
     */
    libmodbusRAWWarper(const char *device = "",
                       int baud = 115200,
                       char parity = 'N',
                       int data_bit = 8,
                       int stop_bit = 1);
    ~libmodbusRAWWarper();

    /**
     * @brief Открыть соединение с устройством
     * @param address Адрес устройства, с которым будет открыто соединение
     * @return код ошибки
     * <table>
     *      <tr><th>0     <td>Удачно
     *      <tr><th>-1    <td>Не создано экзампляра управляющей структуры modbus_t (внутренняя ошибка)
     *      <tr><th>-2    <td>Ошибка при выполнении @link modbus_connect @endlink (драйвер порта сообщает об ошибке)
     * </table>
     */
    int connect(char address);

    /**
     * @brief Закрыть соединение с устройством
     */
    void close(void);

    /**
     * @brief Сброс буферов драйвера. Стоит использовать после возникновения ошибки
     */
    void flush(void);

    /**
     * @brief Выключатель режима отладки
     * @param true включает отладочные сообщения библиотеки libmodbus
     *        false выключает
     * В отладочном режиме библиотека выводит подробный дамп пердаваемых данных
     */
    void setDebug(bool isDebug);

    /**
     * @brief Установить новый режим передачи по последоватьельному порту
     *
     * Меняет настройки уже открытого порта путем пересоздание экземпляра управляющей структуры modbus_t
     * @param device Строка, содержащее имя последовательного (COM) порта ПК.
     *              Для Windows это обычно COM<x> (x - номер по порядку)
     *              Для Linux это /dev/tty<x><y> (x - тип устройства, например S иди USB, y - номер по порядку)
     * @param baud Скорость пердачи [бод] из стандартного ряда (9600, 38400, 115200 и тп.)
     * @param parity Проверка четности.
     * <table>
     *      <tr><th>'N'      <td>Без проверки четности
     *      <tr><th>'E'      <td>Проверка четности
     *      <tr><th>'O'      <td>Проверка нечетности
     * </table>
     * @param data_bit количество бит данных: от 5 до 8
     * @param stop_bit количество стоп-битов: 1 или 2
     * @return Код ошибки
     * <table>
     *      <tr><th>0     <td>Удачно
     *      <tr><th>1     <td>Ошибка.
     * </table>
     * В случае ошибки работа функций передачи будет заблокирована до новой попытки открыть порт.
     */
    char SetUARTMode(const char *device, int baud, char parity,
                            int data_bit, int stop_bit);

    /**
     * @brief Отправить сформированый пользователем пакет данных
     *
     * Использует функцию modbus_send_raw_request() для отправки пользовательского пакета
     * возможно содержащий нестандартный запрос. исходя из режима учета контрольных сумм
     * к пакету будет добавление контрольная сумма, вычисленная по алгоритму CRC16.
     * @param Data Указатель на буфер откуда будет происходить предача
     * @param Length Количество байт в буфере.
     * @return 0 в случае успеха.
     * @sa SetUseCRC16
     */
    unsigned char SendRAWData(unsigned char* Data, unsigned char Length);

    /**
     * @brief Получить неразобранный пакет.
     *
     * Отправляет в указанный буфер все байты принятого пакета кроме конторльной суммы, если она используется.
     * Обычно работает в паре с @link SendRAWData @endlink
     * @param DataPlace Буфер - приемник (должен быть подходящего размера)
     * @return Количество принятых байт.
     * @sa SetUseCRC16
     */
    char ReceiveRAWData(unsigned char* DataPlace);

    /**
     * @brief Установить режим использования контрольной суммы.
     *
     * Режим проверки ВКЛЮЧЕН по умолчанию и соответствует стандарту modbus. Однако некоторые нестандартные
     * Устройства требуют его отключения. При этом к отправляемым пакетам не будет добавляться два байта CRC16,
     * а последние два байта принятого пакета не будут считаться суммой CRC16
     * @param isUseCRC16 true - включить
     *       <br>false - выключить
     */
    void SetUseCRC16(bool isUseCRC16);

    /**
     * @brief Проверка, открыть ли порт.
     * @return true, если порт сейчас открыт.
     * @sa connect
     * @sa close
     */
    bool isOpend(void);

    /**
     * @brief Получить адрес устройства, к которому подключен сейчас
     * @return > 0 адрес, с которым был последний раз вызван метод @link connect @endlink
     * @return 0 если порт закрыт или произошла какая-либо ошибка, после которой связь оборвалась
     */
    unsigned char getOpenedAddress(void);

    /**
     * @brief Установить функцию-хук (перехватчик)
     *
     * Нужно только при работе с нестандартными устройствами. Библиотека modbus анализирует принимаемый пакет "на лету",
     * Чтобы определить конец пакета, однако при работе с нестандартными пакетами она этого сделать не может
     * по причине отсутствия информации о нестандартном устройстве. Поэтому следует указать некоторую функцию, которая
     * будет производить данный подсчет вместо стандартного метода.
     * @param pfHoockFun Указатель на хук-функцию.
     *  <br><br><b>Хук-функция имеет два аргумента:</b>
     *  @param buff буфер, куда принято начало пакета
     *  @param ResivedLength количество уже принятых байт
     *  @return Должна вернуть ожидаемоек количество байт до конца пакета, не включая контрольную сумму,
     *      не зависимо от того, используется она или нет.
     */
    void SetHoock(
            unsigned char(*pfHoockFun)(unsigned char* buff,
                                       char ResivedLength));

    /**
     * @brief Вернуть указатель на управляющую структуру modbus_t
     *
     * Используется, для обращения к стандартным функциям библиотеки libmodbus
     * @return Указатель на управляющую структуру modbus_t, которая соответствут текущему экземпляру libmodbusRAWWarper
     *
     * <br><b>Например:</b>
     * @code
     * modbus_write_registers(mb->getModbusContext(), start_ddress, Write_len, data)
     * @endcode
     */
    modbus_t* getModbusContext(void) const;

private:
    /**
     * @brief Внутренний хук библиотеки modbus, вычисляющий длину блока метаданных в зависимости от функции
     *
     * Многие нестандартные функции имеют поле [длина_данных_после_кода_функции], данный хук проверяет по коду функции
     * есть-ли такое поле и возвращает его длину
     * @param function код функции modbus
     * @param msg_type тип сообщения
     * @param modbus_instance указатель на управляющую структуру modbus_t, обработчик котрой вызвал хук
     * @return ожидаемую длину блока данных в принимаемом пакете
     */
    static unsigned char MbDataLength_Hoock(int function, msg_type_t msg_type, modbus_t* modbus_instance);
    /**
     * @brief Внутренний хук библиотеки modbus, вычисляющий длину блока данных в зависимости от функции
     * @param msg Указатель на буфер с частично-принятым пакетом
     * @param RessivedLength Количество байт уже принятых и записаных в буфер
     * @param msg_type Тип сообщения
     * @param modbus_instance указатель на управляющую структуру modbus_t, обработчик котрой вызвал хук
     * @return Ожидаемую длину поля [данные] пакета.
     * @return 0, если поле [данные] в пакете данного типа отсутствует
     */
    static int MbDataAfterMeta_Hoock(uint8_t *msg, int *RessivedLength,
                                     msg_type_t msg_type, modbus_t* modbus_instance);

    static QMap<modbus_t*, libmodbusRAWWarper*> instanceMap;

    modbus_t *ModBus;
    char isUseCRC16;

    unsigned char buff[256];

    QMutex mutex;

    unsigned char (*_pfHoockFun)(unsigned char* buff, char ResivedLength);
    unsigned char DatafieldLen;

    char fOpend;
    unsigned char OpenAddress;
};

#endif /* LIBMODBUSWARPER_H_ */

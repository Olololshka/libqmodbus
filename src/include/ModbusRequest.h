
/**
 * @file ModbusRequest.h
 *
 * Содержит описание класса ModbusRequest
 */

#ifndef MODBUSREQUEST_H_
#define MODBUSREQUEST_H_

#include <stdint.h>
#include <QObject>
#include <QByteArray>
#include <QMutex>
#include <QList>

/**
 * @brief Объединение для преобразования массива четырех байт в число с плавающей точкой и обратно
 */
union ByteArray2float
{
    char raw[4];
    float result;
};

/**
 * @brief Объединение для преобразования массива четырех байт в два шестнадцатибитных целых и обратно
 */
union ByteArray2Twoint16
{
    char raw[4];
    uint16_t uint[2];
};

/**
 * @brief Объединение для преобразования массива двух байт в шестнадцатибитное целое и обратно
 */
union ByteArray2short
{
    char raw[2];
    uint16_t result;
};

/**
 * @brief Класс обобщенного запроса через интерфейс modbus
 *
 * Упрощает обращение к функциям интерфейса modbus при многократных повторениях одинаковых операций.
 * Расширяет Функционал сигнальной системой Qt
 */
class ModbusRequest : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Тип запроса
     */
    enum enRequestType
    {
        //! Пустой запрос (игнорируется при обработке)
        MB_REQUEST_TYPE_EMPTY,
        //! Нестандартный пользовательский запрос с использованием перехвата
        MB_REQUEST_TYPE_RAWREQUEST_WITH_HOCK,
        //! Нестандартный пользовательский запрос
        MB_REQUEST_TYPE_RAWREQUEST,
        //! Прочитать одну катушку
        MB_REQUEST_TYPE_READ_SINGLE_COIL,
        //! Прочитать несколько подрядидущих катушек
        MB_REQUEST_TYPE_READ_MULTI_COIL,
        //! Записать катушку
        MB_REQUEST_TYPE_WRITE_SINGLE_COIL,
        //! Записать несколько подрядидущих катушек
        MB_REQUEST_TYPE_WRITE_MULTI_COIL,
        //! Прочитать несколько подрядидущих регистров  хранения
        MB_REQUEST_TYPE_READ_HOLDING_REGISTERS,
        //! Записать несколько подрядидущих регистров  хранения
        MB_REQUEST_TYPE_WRITE_HOLDING_REGISTERS,
        //! Прочитать несколько подрядидущих входных регистров
        MB_REQUEST_TYPE_READ_INPUTS,
        //! Прочитать несколько подрядидущих дискретных входов
        MB_REQUEST_TYPE_READ_DISCRET_INPUTS,
        //! Записать регистр хранения
        MB_REQUEST_TYPE_WRITE_HOLDING_REGISTER,
    };
    /**
     * @brief Коды ошибок
     */
    enum ErrorCode
    {
        //! Нет ошибки
        ERR_OK = 0x00,
        //! Функция не поддерживается
        ERR_ILLEGAL_FUNCTION = 0x01,
        //! Попытка обращения к несуществубщему адресу
        ERR_ILLEGAL_DATA_ADDRESS = 0x02,
        //! Данные не соответствуют функции
        ERR_ILLEGAL_DATA_VALUE = 0x03,
        //! Внутренняя ошибка в устройстве при выполнении запроса
        ERR_SLAVE_DEVICE_FAILURE = 0x04,
        //! Начата долговременная операция
        ERR_ACKNOWLEDGE = 0x05,
        //! Устройство занято и не может выполнить запрос сейчас
        ERR_SLAVE_DEVICE_BUSY = 0x06,
        //! Последовательный порт не открыт (ошибка связи)
        ERR_NOT_OPENED = 0xfe,
        //! Неизвестная ошибка
        ERR_UNKNOWN = 0xff
    };

    typedef unsigned char(*_pfHoockFun)(unsigned char*, char);

    /**
     * @brief Обобщенный конструктор
     * @param DeviceAddress Адрес устройства, к которому будет производиться созваваемый запрос
     * @param type тип запроса @link enRequestType @endlink
     * @param request Запрос (сформированный массив байт)
     * @param normalAnsverLen Ожидаемая длина ответа (только для нестандартных запросов)
     * @param HoockFun Указатель на функцию-хук @link SetHoock @endlink
     * @param parent "родитель"
     */
    ModbusRequest(unsigned char DeviceAddress = 255, enum enRequestType type =
            ModbusRequest::MB_REQUEST_TYPE_EMPTY, const QByteArray & request =
            QByteArray(1, 0x00), unsigned char normalAnsverLen = 0,
                  _pfHoockFun HoockFun = NULL, QObject* parent = NULL);

    /**
     * @brief Упрощенный конструктор для создания запроса чтения входных регистров
     * @param DeviceAddress Адрес устройства, к которому будет производиться созваваемый запрос
     * @param startAddress Адрес начального входа
     * @param Length Количество читаемых входов
     * @return Указатель на созданный обьект ModbusRequest.
     * @note Удалять обьект-запрост, созданный таким бразом нужно вручную
     * <br><b>Например:</b>
     * @code
     * delete somerequest;
     * @endcode
     * или
     * @code
     * somerequest->deleteLeter();
     * @endcode
     */
    static ModbusRequest* BuildReadInputsRequest(unsigned char DeviceAddress =
            255, uint16_t startAddress = 0x0000, uint16_t Length = 0x0001); 
    /**
     * @brief Упрощенный конструктор для создания запроса чтения катушек
     * @param DeviceAddress Адрес устройства, к которому будет производиться созваваемый запрос
     * @param startAddress Адрес начальной катушки
     * @param Length Количество читаемых катушек
     * @return Указатель на созданный обьект ModbusRequest.
     * @note Удалять обьект-запрост, созданный таким бразом нужно вручную
     * <br><b>Например:</b>
     * @code
     * delete somerequest;
     * @endcode
     * или
     * @code
     * somerequest->deleteLeter();
     * @endcode
     */
    static ModbusRequest* BuildReadCoilsRequest(unsigned char DeviceAddress =
            255, uint16_t startAddress = 0x0000, uint16_t Length = 0x0001);
    /**
     * @brief Упрощенный конструктор для создания запроса чтения регистров хранениея
     * @param DeviceAddress Адрес устройства, к которому будет производиться созваваемый запрос
     * @param startAddress Адрес начального регистра
     * @param Length Количество читаемых регистров
     * @return Указатель на созданный обьект ModbusRequest.
     * @note Удалять обьект-запрост, созданный таким бразом нужно вручную
     * <br><b>Например:</b>
     * @code
     * delete somerequest;
     * @endcode
     * или
     * @code
     * somerequest->deleteLeter();
     * @endcode
     */
    static ModbusRequest* BuildReadHoldingRequest(unsigned char DeviceAddress =
            255, uint16_t startAddress = 0x0000, uint16_t Length = 0x0001);
    /**
     * @brief Упрощенный конструктор для создания запроса на запись ОДНОЙ катушки
     * @param DeviceAddress Адрес устройства, к которому будет производиться созваваемый запрос
     * @param CoilAddress Адрес записываемой катушки
     * @param NewState Записываемое значение
     * @return Указатель на созданный обьект ModbusRequest.
     * @note Удалять обьект-запрост, созданный таким бразом нужно вручную
     * <br><b>Например:</b>
     * @code
     * delete somerequest;
     * @endcode
     * или
     * @code
     * somerequest->deleteLeter();
     * @endcode
     */
    static ModbusRequest* BuildWriteMultiCoilRequest(
            unsigned char DeviceAddress = 255, uint16_t StartCoil = 0x0000,
            const QByteArray &CoilsStates = QByteArray(1, 0));
    /**
     * @brief Упрощенный конструктор для создания запроса на запись множества катушек
     * @param DeviceAddress Адрес устройства, к которому будет производиться созваваемый запрос
     * @param StartCoil Адрес записываемой катушки
     * @param CoilsStates Записываемые значения
     * @return Указатель на созданный обьект ModbusRequest.
     * @note Удалять обьект-запрост, созданный таким бразом нужно вручную
     * <br><b>Например:</b>
     * @code
     * delete somerequest;
     * @endcode
     * или
     * @code
     * somerequest->deleteLeter();
     * @endcode
     */
    static ModbusRequest* BuildWriteSingleCoilRequest(
            unsigned char DeviceAddress = 255, uint16_t CoilAddress = 0x0000,
            bool NewState = false);
    /**
     * @brief Упрощенный конструктор для создания запроса на запись регистров хранения
     * @param DeviceAddress Адрес устройства, к которому будет производиться созваваемый запрос
     * @param StartAddress Начальный адрес записи
     * @param data Записываемые данные (длина кратна двум байтам)
     * @return Указатель на созданный обьект ModbusRequest.
     * @note Удалять обьект-запрост, созданный таким бразом нужно вручную
     * <br><b>Например:</b>
     * @code
     * delete somerequest;
     * @endcode
     * или
     * @code
     * somerequest->deleteLeter();
     * @endcode
     */
    static ModbusRequest* BuildWriteHoldingRequest(unsigned char DeviceAddress =
            255, uint16_t StartAddress = 0x0000, const QByteArray & data =
            QByteArray(1, 0));

    /**
     * @brief Упрощенный конструктор для создания запроса на чтение дискретных входов
     * @param DeviceAddress Адрес устройства, к которому будет производиться созваваемый запрос
     * @param StartAddress Начальный адрес записи
     * @param Length количествочитаемых входов
     * @return Указатель на созданный обьект ModbusRequest.
     * @note Удалять обьект-запрост, созданный таким бразом нужно вручную
     * <br><b>Например:</b>
     * @code
     * delete somerequest;
     * @endcode
     * или
     * @code
     * somerequest->deleteLeter();
     * @endcode
     */
    static ModbusRequest* BuildReadDiscretInputsRequest(unsigned char DeviceAddress =
            255, uint16_t StartAddress = 0x0000, uint16_t Length = 0x0001);

    ModbusRequest(const ModbusRequest & source);
    ModbusRequest & operator =(const ModbusRequest & source);
    /**
     * @brief Оператор сравнения
     * @return true в случае совпадения запросов. Поле ответа при этом не учитывается.
     * Поэтому еоднажды обработанный запрос будет эквиывлентен вновь созданному с теми же параметрами
     */
    bool operator ==(const ModbusRequest & right);
    virtual ~ModbusRequest();

    /**
     * @brief Изменить адрес назначения уже созданному запросу
     *
     * Потокобезопасный метод.
     * @param DeviceAddress Новый адрес устройства назначения
     */
    void setDeviceAddress(unsigned char DeviceAddress);

    /**
     * @brief getDeviceAddress получить адрес устройства назначения
     *
     * Потокобезопасный метод.
     * @return Адрес устройства назначения объекта-запроса
     */
    unsigned char getDeviceAddress(void) const;

    /**
     * @brief Изменить тип запроса
     *
     * Осторожно, поле дополнительных данных при этом не изменяется, что может вызвать ошибки!
     * Потокобезопасный метод
     * @param reqType код типа запроса @link enRequestType @endlink
     */
    void setRequestType(enum enRequestType reqType);
    /**
     * @brief Получить тип запроса
     *
     * Потокобезопасный метод
     * @return Тип запроса @link enRequestType @endlink
     */
    enRequestType getRequestType(void) const;

    /**
     * @brief Установить байты данных запроса
     *
     * Изменяет поле [длины] данных и поле [данные], на новые упакованные в один массив.
     * Использовать с осторожностью.
     * Потокобезопасный метод.
     * @param requestData
     */
    void setRequest(const QByteArray & requestData);

    /**
     * @brief Получить данные запроса
     *
     * Потокобезопасный метод.
     * @return Массив, содержащий поле [длина] и [данные]
     */
    QByteArray getRequest(void) const;

    /**
     * @brief Установить значение поля ответа
     *
     * Используется для внутренних целей. Записывает в обьект данные ответа от устройства.
     * Потокобезопасный метод.
     * @param AnsverData
     */
    void setAnsver(const QByteArray & AnsverData);

    /**
     * @brief Получить данные ответа
     *
     * Потокобезопасный метод.
     * @return Массив байт ответа от устройства не включая поля [код функции], [длина ответа] и [контрольная сумма]
     */
    QByteArray getAnsver(void) const;

    /**
     * @brief Установить код ошибки последней операции
     *
     * Для внутреннего использования.
     * Потокобезопасный метод.
     * @param errCode
     */
    void setErrorCode(enum ErrorCode errCode);

    /**
     * @brief Получить код ошибки.
     *
     * Получить код последней ошибки при операции над запросом.
     * Потокобезопасный метод.
     * @return Код опшибки @line ErrorCode @endlink
     * <br><b>Нпример:</b>
     * ModbusRequest* readreq = BuildReadHoldingRequest(0x03, 0x0010, 0x0002); // создание запроса на чтение двух ячеек хранения
     * engine->SyncRequest(*readreq); // выполнить запрос
     * if (readreq->getErrorrCode() != ModbusRequest::ERR_OK) // проверка успешности запроса
     *  {
     *  // ошибка
     *  }
     * else
     *  {
     *  // успешно, продолжить работу.
     *  }
     * @sa SyncRequest
     */
    enum ModbusRequest::ErrorCode getErrorrCode(void) const;

    /**
     * @brief Получить указатель на хук-функцию
     *
     * Возвращает на указатель на хук-функцию, используемую в libmodbusRAWWarper для нестандартных запросов.
     * Потокобезопасный метод.
     * @sa getNormalAnsverLen
     * @return Укадатель на хук-функцию, если используется.
     * @return NULL, если хук-функция не испольщуется.
     */
    _pfHoockFun getpfHoockFun(void) const;

    /**
     * @brief Установить хук-функцию и включить её использование
     *
     * Функция используется как обобщенный хук для libmodbusRAWWarper для нестандартных запросов
     * Потокобезопасный метод.
     * @sa getNormalAnsverLen
     * @param fun Указатель на хук-функцию
     */
    void setpfHoockFun(_pfHoockFun fun);

    /**
     * @brief Ожидаемая длина ответа
     *
     * Используется при обработке нестандартных запросов. Если хук-функция установлена, то для расчера используется она
     * Иначе возвращается содержимое служебного поля, NormalAnsverLen задаваемого в конструкторе или функцией
     * Потокобезопасный метод.
     * @link setNormalAnsverLen @endlink
     * @return Ожидаемую суммарную длину полей [длина ответа] + [данные ответа]
     */
    unsigned char getNormalAnsverLen() const;

    /**
     * @brief Установить ожидаемую длину ответа
     *
     * Установить ожидаемую длину ответа на нестандартный запрос, тоесть суммарную длину полей [длина ответа] + [данные ответа]
     * Потокобезопасный метод.
     * @param normalAnsverLen суммарная длина ответа [длина ответа] + [данные ответа]
     */
    void setNormalAnsverLen(unsigned char normalAnsverLen);

    /**
     * @brief Проверка разрешениея на обработку.
     *
     * Проверяет разрешение на обработку данного запроса. Уменьшает на единицу счетчик-делитель обработки запроса.
     * @return true - Обработка разрешена
     * @return false - Обработка запрещена. Попытка обработки данного запроса не приведет к реальной предаче данных
     * @sa setEnabled
     */
    bool checkEnabled();

    /**
     * @brief Проверка разрешениея на обработку.
     *
     * Проверяет разрешение на обработку данного запроса.
     * @return true - Обработка разрешена
     * @return false - Обработка запрещена. Попытка обработки данного запроса не приведет к реальной предаче данных
     */
    bool isEnabled() const;

    /**
     * @brief Установить разрешение и счетчик-делитель разрешения
     * @param enabled
     * <table>
     *      <tr><th>0           <td>Запрос разрешен к выполнениею
     *      <tr><th>больше 0    <td>Запрс разрешен один из enabled раз (делитель)
     *      <tr><th>меньше 0    <td>Запрос полностью запрещен
     * </table>
     */
    void setEnabled(int enabled);

    /**
     * @brief Представить ответ как последовательность чисел типа float
     *
     * Упращяет преобразование последовательности байт ответа в числа с плавающей точкой.
     * @note Длина ответа должна быть кратна 4 байтам (2 ячейки modbus)
     * @return Список чисел с плавающкй точкой, длина списка равна <Длина ответа>/4
     * <br><b>Например:</b>
     * @code
     * ModbusRequest* readreq = BuildReadInputRequest(0x01, 0x0000, 0x0006); // Создание запроса на шести входных ячеек
     * engine->SyncRequest(*readreq); // выполнить запрос
     * if (readreq->getErrorrCode() != ModbusRequest::ERR_OK) // проверка успешности запроса
     *  {
     *  // ошибка
     *  }
     * else
     *  {
     *  //преобразование ответа в список чисел с плавающей точкой
     *  QList<float> result = readreq->getAnsverAsFloat();
     *  // некоторый вывод результата
     *  printf("X = %.2f, Y = %.2f, Z = %.2f", result[0], result[1], result[2]);
     *  }
     * @code
     * @sa SyncRequest
     */
    QList<float> getAnsverAsFloat(void) const;

    /**
     * @brief Представить ответ как последовательность беззнаковых шестнадцатибитных целых чисел
     *
     * Упращяет преобразование последовательности байт ответа в беззнаковые целые числа (0..65535).
     * @note Длина ответа должна быть кратна 2 байтам
     * @return Список беззнаковых целых чисел, длина списка равна <Длина ответа>/2
     */
    QList<uint16_t> getAnsverAsShort(void) const;

    /**
     * @brief Ускуственно вызвать слот запроса.
     *
     * Используется в отладочных целях. Позволяет принудительно вызвать слот @link updated @endlink
     * и соответственно некоторые связанные с ним слоты без реального выполнения запроса к физическому
     * устройству
     */
    void EmitSignal();

signals:
    /**
     * @brief Сигнал, вызываемый после завершения выполнения запроса ядром @link ModbusEngine @endlink
     *
     * @note Сигнал будет вызван только в асинхронном режиме, тоесть если он добавлен в очередь автовызова.
     * @sa AddAutocallRequest
     */
    void updated();

private:
    unsigned char DeviceAddress;
    enum enRequestType RequestType;
    //эти поля содержит разные даные, в зависимости от enRequestType
    QByteArray Request;
    QByteArray Ansver;
    _pfHoockFun pfHoockFun; // хук-функция для режима MB_REQUEST_TYPE_RAWREQUEST_WITH_HOCK
    enum ErrorCode err;
    unsigned char normalAnsverLen; // необязательное поле, используется, когда pfHoockFun == NULL
    mutable QMutex Mutex;

    int Enabled; // > счетчик/делитель 0 - разрешено <0 - запрещено
    int devider; // делитель
};

#endif /* MODBUSREQUEST_H_ */

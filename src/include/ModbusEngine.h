/**
 * @file ModbusEngine.h
 * @brief Содержит описане класса ModbusEngine
 */

#ifndef MODBUSENGINE_H_
#define MODBUSENGINE_H_

#include <QThread>
#include <QByteArray>
#include <QList>
#include <modbus/modbus.h>

#include "ModbusRequest.h"

class QTimer;
class QMutex;
class libmodbusRAWWarper;

/**
 * @brief Класс предоставляющий синхронно-асинхронное ядро modbus
 *
 * Класс оперирует с обьектами-запросами @link ModbusRequest @endlink
 */
class ModbusEngine: public QThread
    {
Q_OBJECT

public:
    ModbusEngine();
    ~ModbusEngine();

    /**
     * @brief Добавить запрос в очередь автообновления
     *
     * Добавляет указатель на запрос в очередь автообновления.
     * Исходя из настроек он будет автоматически выполняться с определенным интервалом до момента его
     * исключения из очереди или остановки автообновления.
     * @param request Указатель на объект @link ModbusRequest @endlink
     * @note
     *      1. Добавленый в очередь обьект становится "потомком" текущего обьекта и его ручнон унечтожение
     *          больше не требуется.
     *      2. Не унечтожайте добавленый в очередь автообновления запрос, это вызовет крах программы.
     * @sa AddListOfAutocallRequests
     * @sa RemoveAutocallRequest
     * @sa RemoveListOfAutocallRequests
     */
    void AddAutocallRequest(ModbusRequest* request);
    /**
     * @brief Добавить список запросов в очередь автообновления
     *
     * Аналогично @link AddAutocallRequest @endlink, но добавляет в очередь сразу ренее сформирований список запросов
     * @param requestList Список запросов, добавляемых в очередь автообновления
     * @sa AddListOfAutocallRequests
     * @sa RemoveAutocallRequest
     * @sa RemoveListOfAutocallRequests
     */
    void AddListOfAutocallRequests(QList<ModbusRequest*> &requestList);

    /**
     * @brief Удалить запрос из списка автообновления
     *
     * Удаленный из списка запрос перестает цыклически обрабатываться.
     * Также он становится потомком NULL, поэтому требуется его ручное унечтожение.
     * @param request указатель на удаляемй из очереди автообновления запрос
     * @return true - операция удаления успешна
     * @return false - Удаляемый запрос уже отсутствует в очереди.
     * @sa AddAutocallRequest
     * @sa RemoveAutocallRequest
     * @sa RemoveListOfAutocallRequests
     */
    bool RemoveAutocallRequest(ModbusRequest* request);

    /**
     * @brief Удалить из очереди автообновления список запросов
     *
     * Аналогично @link RemoveAutocallRequest @endlink, но работает со списком запросов
     * @param requestList Список удаляемых из очереди автообновления запросов
     * @return Количество успешно удаленных запросов.
     * @sa AddAutocallRequest
     * @sa AddListOfAutocallRequests
     * @sa RemoveAutocallRequest
     */
    int RemoveListOfAutocallRequests(QList<ModbusRequest*> &requestList);

    // приостанавливает автообновление и срочно выполняет запрос, сразу возвращает ответ
    /**
     * @brief Выполнить синхронный запрос
     *
     * Выполнение программы будет задержано на время медленной операции I/O, но ответ будет доступен сразу.
     * Используется для редких неповторяющихся операций или операций, результат которых влияет на дальнейшее
     * поведение программы. Для регулярного обновления данных используйте асинхронный механизм.
     * Потокобезопастный метод.
     * @param request Сформированая структура запроса.
     * @return Код ошибки
     * <table>
     *      <tr><th>0           <td>Запрос Выполнен (ошибки в ответе не учитываются здесь)
     *      <tr><th>-1          <td>Не создана управляющая структура modbus_t
     *      <tr><th>-2          <td>Невозможно открыть порт, ошибка вида <br>'ERROR Can't open the device COMx (Нет такого файла или каталога)
     *      <tr><th>-3          <td>Не создано экземпляра libmodbusRAWWarper
     * </table>
     * @note Потокобезопастность данного метода предусматривает принцип конкуренции между запросами: "кто первый встал - того и тапки".
     * Тоесть каждый следующий запрос будет ждать завершения обработки предидущего.
     */
    int SyncRequest(ModbusRequest& request);
    /**
     * @brief Установить задержку между циклами автообновления
     *
     * Данная задержка начинается ПОСЛЕ завершения обработки последнего запроса в очереди, поэтому влияет на ПЕРИОД
     * автообновления только косвенно в зависимости от количества запросов в очереди и времени выполнения каждого из них
     * Для времекритичных операций используейте сочетание таймер + @link SyncRequest @endlink.
     * Потокобезопасный метод.
     * @param NewDelay Время задержки между цыклами автообновления в милисекундах (по умолчанию: 1).
     */
    void SetCycleDelay(unsigned long NewDelay = 0);

    /**
     * @brief Проверка глобального разрешения цикла автообновления.
     * @return  true Автообновление разрешено и работает.
     *          false Автообновление остановлено.
     */
    bool isAutoupdateGlobalEnabled() const;

    /**
     * @brief "Выключатель" автообновления
     *
     * Разрешает/запрещает цикл автообновления.
     * @param AutoupdateGlobalEnabled   true - Разрешить работу цикла патообновления
     *                                  false - Запретить и остановить цикл патообновления
     */
    void setAutoupdateGlobalEnabled(bool autoupdateGlobalEnabled);
	
	/**
     * @brief Получить указатель на управляющу структуру modbus_t
     *
     * @return Указатель на управляющу структуру modbus_t
     */
	modbus_t* getModbusContext() const;

    /**
     * @brief Устанавливает режим закрытия порта, после возникновения первой ошибки
     *
     * Разрешает/запрещает закрытие порта, после возникновения первой ошибки.
     * Включено по-умолчанию.
     * @param v   true - Разрешить закрытие порта, после возникновения первой ошибки
     *            false - Запретить закрытие порта, после возникновения первой ошибки
     */
    void setTryCloseonError(bool v = true);

public slots:
    /**
     * @brief Слот презапуска соединениея
     *
     * Пересоздает обьект @link libmodbusRAWWarper @endlink с новыми настройками передачи, которые читает из
     * глобального обьекта SettingsCmdLine::settings библиотеки libsettingscmdline, экземпляр которой должен быть
     * инициализирован.
     */
    void RestartConnection(void); // перечитать настройки, перезапустить прдключение
    /**
     * @brief Слот Установки таймаута
     *
     * Устанавливает новое значение таймаута примена в бэкэнде libmodbus при помощи функции modbus_set_response_timeout()
     */
    void setTimeout(unsigned int timeout_ms);
    /**
     * @brief Слот, принудительно закрывает порт
     * Закрывает порт, Он будеет автоматически открыт при выполнении следующего запроса
     */
    void close();

private:
    QList<ModbusRequest*> AutocallRequests;
    mutable QMutex OperationInProgressMutex; // этот мьютекс предотвращает выполнение более 1 запроса за раз (автоапдейта и синхронного)
    mutable QMutex UpdateDelayMutex;
    mutable QMutex UpdateEnableMutex;
    mutable QMutex ProcessTerminateMutex;
    libmodbusRAWWarper *mb;
    bool AutoupdateGlobalEnabled;
    unsigned int AdditionalUpdateSycleDelay;
    unsigned long getCycleDelay(void);
    static unsigned char defaultHoockfun(unsigned char *buff, char resiverlen);
    static unsigned char currentNormalAnsverLen;
    bool tryCloseonError;

protected:
    /**
     * @brief Точка входа потока автообновления
     */
    void run(void);

signals:
    /**
     * @brief Сигнал, вызываемый по окончании каждого цикла автообновления
     */
    void AutoupdateComplead(void);

    /**
     * @brief Сигнал сообщающий об неисправимой ошибке связи, например отказ последовательного порта.
     * @param Код ошибки
     *      <tr><th>1          <td>Не создана управляющая структура modbus_t
     *      <tr><th>2          <td>Невозможно открыть порт, ошибка вида <br>'ERROR Can't open the device COMx (Нет такого файла или каталога)
     */
    void ModbusGeneralFailure(int);
    };

#endif /* MODBUSENGINE_H_ */

/**
 * @file common.h
 * @brief Содержит объявление некоторых частоиспользуемых макросов
 */

#ifndef COMON_H_
#define COMON_H_

#include <assert.h>

//! @briefМакросс NULL.
//!
//! Некоторые IDE не могут его распознать по умолчанию и заявляют об ошибке.
//! Данное обьявление ликвидилует проблему
#ifndef NULL
#define NULL	(void*)0
#endif

//! Макросы упарвления флагами/битами
//!
//! @{
/**
 * @brief Установить флаг(-и)
 * @arg Flags целочисленная переменная, используемая как контейнер флагов
 * @arg Flag маска флагов, которые будут УСТАНОВЛЕНЫ
 *
 * <br><b>Например:</b>
 * @code
 * int a = 0b101;
 * _SET_FLAG(a, (1 << 1) | (1 << 5));
 * // a == 0b100111
 * @endcode
 */
#define _SET_FLAG(Flags, Flag)   	(Flags |= (Flag))
/**
 * @brief Снять флаг(-и)
 * @arg Flags целочисленная переменная, используемая как контейнер флагов
 * @arg Flag маска флагов, которые будут СНЯТЫ
 *
 * <br><b>Например:</b>
 * @code
 * int a = 0b111;
 * _SET_FLAG(a, (1 << 0) | (1 << 2));
 * // a == 0b10
 * @endcode
 */
#define _UNSET_FLAG(Flags, Flag) 	(Flags &= ~(Flag))
/**
 * @brief Сменить состояние флага(/ов)
 * @arg Flags целочисленная переменная, используемая как контейнер флагов
 * @arg Flag маска флагов, которые будут СЕНЕНЫ
 *
 * <br><b>Например:</b>
 * @code
 * int a = 0b110;
 * _SET_FLAG(a, (1 << 0) | (1 << 2));
 * // a == 0b11
 * @endcode
 */
#define _TOUGLE_FLAG(Flags, Flag) 	(Flags ^= (Flag))
/**
 * @brief Проверить флаг
 *
 * Проверяет ОДИН флаг и возвращает true в случае совпадениея с проверяемым значением
 * @arg Flags целочисленная переменная, используемая как контейнер флагов
 * @arg Flag маска, содержащя '1' на месте проверяемого бита.
 * @arg State Проверяемое значение false или true
 * @return true в случае совпадения @link State @endlink и проверяемого бита
 *
 * <br><b>Например:</b>
 * @code
 * int a = 2; // 0b10
 * bool res1 = _CHECK_FLAG(a, (1 << 0), 1); // false
 * bool res2 = _CHECK_FLAG(a, (1 << 1), 1); // true
 * @endcode
 */
#define _CHECK_FLAG(Flags, Flag, State) ((Flags & (Flag)) ? (State) : (!State))
//! @}

/**
 * @brief Отладочное удаление.
 *
 * Удаляет объект по указателю, при этом проверяется, что значение указателя отлично от @link NULL @endlink.
 * После удаления значение указателя приравнивается к @link NULL @endlink.
 * При попытке двойного удаления будет выведено сообщение об ошибке.
 */
#define __DELETE(x)			{\
					assert(x != NULL);\
					delete  x;\
					x = NULL;\
					}

#endif /* COMON_H_ */

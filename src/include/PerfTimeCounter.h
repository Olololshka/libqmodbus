/**
 * @file PerfTimeCounter.h
 * @brief Содержит описание вспомогательного класса, оценивающего задержки в выполнении программы
 */

#ifndef PERFTIMECOUNTER_H_
#define PERFTIMECOUNTER_H_


/**
 * @brief Класс счетчика/захвата системного времени.
 *
 * Используется для вычисления задержки между событиями (для отладки)
 */
class PerfTimeCounter
{
public:
    PerfTimeCounter();
    virtual ~PerfTimeCounter();

    /**
     * @brief Сбросить счетчик и начать отсчет
     */
    void Start(void);

    /**
     * @brief "Захват" значения счетчика
     * @return Количество милисекунд, прошедших с вызова метода Start()
     */
    unsigned long Catch();

private:
    /**
     * @brief Получить системное время
     * @return Системное время в милисекундах
     */
    unsigned long GetTickCount(void);

    unsigned long starttime;
};

#endif /* PERFTIMECOUNTER_H_ */

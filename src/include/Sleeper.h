/**
 * @file Sleeper.h
 * @brief Статический класс, открывающий доступ к функции задержки, которая по умолчанию protected в QThread
 */

#ifndef SLEEPER_H_
#define SLEEPER_H_

#include <QThread>

/**
 * @brief Класс генерации задержки
 */
class Sleeper: public QThread
{
    Q_OBJECT
public:
    /**
     * @brief Генерация задержка
     *
     * Останавливает выполнение вызвавшего этот метод потока на указанное время
     * @param ms Время задержки в милисекундах
     */
    static void msleep(int ms);

public slots:
    /**
     * @brief Слот генерации задержки. Нужен для доступа из QScript (если используется).
     *
     * Работает аналогично @link msleep @endlink, но может быть вызван из QScript или через соединение
     * сигнал - слот.
     * @param ms Время задержки в милисекундах
     */
    static void sleep_ms(int ms);
};

#endif /* SLEEPER_H_ */

/*
 * ModbusRequest.cpp
 *
 *  Created on: 07.06.2012
 *      Author: tolyan
 */

#include <cmath>
#include <QMutex>
#include <math.h>

#include "common.h"

#include "ModbusRequest.h"

ModbusRequest::ModbusRequest(unsigned char DeviceAddress,
                             enum enRequestType type, const QByteArray & request,
                             unsigned char normalAnsverLen, _pfHoockFun HoockFun, QObject* parent) :
    QObject(parent)
{
    this->DeviceAddress = DeviceAddress;
    RequestType = type;
    Request = request;
    Request.detach();
    pfHoockFun = HoockFun;
    this->normalAnsverLen = normalAnsverLen;
    err = ERR_OK;
    Enabled = 0;
    devider = 0;
}

ModbusRequest::ModbusRequest(const ModbusRequest& source)
{
    source.Mutex.lock();
    this->DeviceAddress = source.DeviceAddress;
    this->Request = source.Request;
    this->Request.detach();
    this->Ansver = source.Ansver;
    this->Ansver.detach();
    this->pfHoockFun = source.pfHoockFun;
    this->RequestType = source.RequestType;
    this->pfHoockFun = source.pfHoockFun;
    this->normalAnsverLen = source.normalAnsverLen;
    this->Enabled = source.Enabled;
    this->devider = source.devider;
    err = ERR_OK;
    source.Mutex.unlock();
}

ModbusRequest& ModbusRequest::operator=(const ModbusRequest& source)
{
    source.Mutex.lock();
    Mutex.lock();
    this->DeviceAddress = source.DeviceAddress;
    this->Request = source.Request;
    this->Request.detach();
    this->Ansver = source.Ansver;
    this->Ansver.detach();
    this->RequestType = source.RequestType;
    this->pfHoockFun = source.pfHoockFun;
    this->err = source.err;
    this->pfHoockFun = source.pfHoockFun;
    this->normalAnsverLen = source.normalAnsverLen;
    this->Enabled = source.Enabled;
    this->devider = source.devider;
    source.Mutex.unlock();
    Mutex.unlock();
    return *this;
}

bool ModbusRequest::operator==(const ModbusRequest& right)
{
    bool res;
    this->Mutex.lock();
    right.Mutex.lock();
    res = ((this->Ansver == right.Ansver) && (this->DeviceAddress
                                              == right.DeviceAddress) && (this->Request == right.Request)
           && (this->RequestType == right.RequestType) && (this->pfHoockFun
                                                           == right.pfHoockFun) && (this->pfHoockFun == right.pfHoockFun)
           && (this->normalAnsverLen == right.normalAnsverLen));
    this->Mutex.unlock();
    right.Mutex.unlock();
    return res;
}

ModbusRequest::~ModbusRequest()
{
}

void ModbusRequest::setDeviceAddress(unsigned char DeviceAddress)
{
    Mutex.lock();
    this->DeviceAddress = DeviceAddress;
    Mutex.unlock();
}

unsigned char ModbusRequest::getDeviceAddress(void) const
{
    unsigned char res;
    Mutex.lock();
    res = DeviceAddress;
    Mutex.unlock();
    return res;
}

void ModbusRequest::setRequestType(enum enRequestType reqType)
{
    Mutex.lock();
    RequestType = reqType;
    Mutex.unlock();
}

ModbusRequest::enRequestType ModbusRequest::getRequestType(void) const
{
    enum enRequestType res;
    Mutex.lock();
    res = RequestType;
    Mutex.unlock();
    return res;
}

void ModbusRequest::setRequest(const QByteArray& requestData)
{
    Mutex.lock();
    Request = requestData;
    Request.detach();
    Mutex.unlock();
}

QByteArray ModbusRequest::getRequest(void) const
{
    QByteArray res;
    Mutex.lock();
    res = Request;
    res.detach();
    Mutex.unlock();
    return res;
}

void ModbusRequest::setAnsver(const QByteArray& AnsverData)
{
    Mutex.lock();
    Ansver = AnsverData;
    Ansver.detach();
    Mutex.unlock();
}

QByteArray ModbusRequest::getAnsver(void) const
{
    QByteArray res;
    Mutex.lock();
    res = Ansver;
    res.detach();
    Mutex.unlock();
    return res;
}

void ModbusRequest::setErrorCode(enum ErrorCode errCode)
{
    Mutex.lock();
    err = errCode;
    Mutex.unlock();
}

ModbusRequest::ErrorCode ModbusRequest::getErrorrCode(void) const
{
    enum ModbusRequest::ErrorCode res;
    Mutex.lock();
    res = err;
    Mutex.unlock();
    return res;
}

ModbusRequest::_pfHoockFun ModbusRequest::getpfHoockFun(void) const
{
    _pfHoockFun res;
    Mutex.lock();
    res = pfHoockFun;
    Mutex.unlock();
    return res;
}

unsigned char ModbusRequest::getNormalAnsverLen() const
{
    unsigned char res;
    Mutex.lock();
    res = normalAnsverLen;
    Mutex.unlock();
    return res;
}

ModbusRequest* ModbusRequest::BuildReadInputsRequest(
        unsigned char DeviceAddress, uint16_t startAddress, uint16_t Length)
{
    union ByteArray2Twoint16 convertTemp;
    convertTemp.uint[0] = startAddress;
    convertTemp.uint[1] = Length;
    return new ModbusRequest(DeviceAddress,
                             ModbusRequest::MB_REQUEST_TYPE_READ_INPUTS, QByteArray(
                                 convertTemp.raw, 4));
}

ModbusRequest *ModbusRequest::BuildReadCoilsRequest(unsigned char DeviceAddress, uint16_t startAddress, uint16_t Length)
{
    union ByteArray2Twoint16 convertTemp;
    convertTemp.uint[0] = startAddress;
    convertTemp.uint[1] = Length;
    return new ModbusRequest(DeviceAddress,
                             ModbusRequest::MB_REQUEST_TYPE_READ_MULTI_COIL, QByteArray(
                                 convertTemp.raw, 4));
}

ModbusRequest *ModbusRequest::BuildReadHoldingRequest(
        unsigned char DeviceAddress, uint16_t startAddress, uint16_t Length)
{
    union ByteArray2Twoint16 convertTemp;
    convertTemp.uint[0] = startAddress;
    convertTemp.uint[1] = Length;
    return new ModbusRequest(DeviceAddress,
                             ModbusRequest::MB_REQUEST_TYPE_READ_HOLDING_REGISTERS, QByteArray(
                                 convertTemp.raw, 4));
}

ModbusRequest *ModbusRequest::BuildWriteMultiCoilRequest(
        unsigned char DeviceAddress, uint16_t StartCoil, const QByteArray &CoilsStates)
{
    QByteArray temp;
    temp.append((char*) &StartCoil, 2); // стартовый адресс
    uint16_t NumOfCells = CoilsStates.size() * 8; // битов
    temp.append((char*) &NumOfCells, 2); // длина (в ячейках)
    temp.append(CoilsStates); // значения
    return new ModbusRequest(DeviceAddress,
                             ModbusRequest::MB_REQUEST_TYPE_WRITE_MULTI_COIL, temp);
}

bool ModbusRequest::checkEnabled()
{
    bool res = false;
    Mutex.lock();
    if (Enabled >= 0)
    {

        if (Enabled == 0)
        {
            Enabled = devider;
            res = true;
        }
        else
            --Enabled;

    }
    Mutex.unlock();
    return res;
}

bool ModbusRequest::isEnabled() const
{
    return (Enabled == 0);
}

ModbusRequest *ModbusRequest::BuildWriteSingleCoilRequest(
        unsigned char DeviceAddress, uint16_t CoilAddress, bool NewState)
{
    union ByteArray2Twoint16 convertTemp;
    convertTemp.uint[0] = CoilAddress;
    convertTemp.uint[1] = (uint16_t) NewState;
    return new ModbusRequest(DeviceAddress,
                             ModbusRequest::MB_REQUEST_TYPE_WRITE_SINGLE_COIL, QByteArray(
                                 convertTemp.raw, 4));
}

ModbusRequest *ModbusRequest::BuildWriteHoldingRequest(
        unsigned char DeviceAddress, uint16_t StartAddress,
        const QByteArray & data)
{
    if (data.length() == 2)
    {
        QByteArray temp;
        temp.append((char*) &StartAddress, 2); // стартовый адресс
        temp.append(data); // значения
        return new ModbusRequest(DeviceAddress,
                                 ModbusRequest::MB_REQUEST_TYPE_WRITE_HOLDING_REGISTER, temp);
    }
    else
    {
        QByteArray temp;
        temp.append((char*) &StartAddress, 2); // стартовый адресс
        uint16_t NumOfCells = data.size() / 2;
        temp.append((char*) &NumOfCells, 2); // длина (в ячейках)
        temp.append(data); // значения
        return new ModbusRequest(DeviceAddress,
                                 ModbusRequest::MB_REQUEST_TYPE_WRITE_HOLDING_REGISTERS, temp);
    }
}

ModbusRequest *ModbusRequest::BuildReadDiscretInputsRequest(unsigned char DeviceAddress, uint16_t StartAddress, uint16_t Length)
{
    union ByteArray2Twoint16 convertTemp;
    convertTemp.uint[0] = StartAddress;
    convertTemp.uint[1] = Length;
    return new ModbusRequest(DeviceAddress,
                             ModbusRequest::MB_REQUEST_TYPE_READ_DISCRET_INPUTS, QByteArray(
                                 convertTemp.raw, 4));
}

void ModbusRequest::setEnabled(int enabled)
{
    Mutex.lock();
    Enabled = enabled;
    devider = enabled;
    Mutex.unlock();
}

void ModbusRequest::setNormalAnsverLen(unsigned char normalAnsverLen)
{
    Mutex.lock();
    this->normalAnsverLen = normalAnsverLen;
    Mutex.unlock();
}

void ModbusRequest::setpfHoockFun(_pfHoockFun fun)
{
    Mutex.lock();
    pfHoockFun = fun;
    Mutex.unlock();
}

QList<float> ModbusRequest::getAnsverAsFloat(void) const
{
    QByteArray ansver = getAnsver();
    QList<float> res;
    if (getErrorrCode() != ERR_OK)
    {
        res.append(NAN);
    }
    else
    {
        if (ansver.length() % 4 != 0)
        {
            res.append(NAN);
        }
        else
        {
            ByteArray2float t;
            for (int i = 0; i < ansver.length(); i += 4)
            {
                memcpy(t.raw, ansver.data() + i, 4);
                res.append(t.result);
            }
        }
    }
    return res;
}

QList<uint16_t> ModbusRequest::getAnsverAsShort(void) const
{
    QByteArray ansver = getAnsver();
    QList<uint16_t> res;
    if (getErrorrCode() != ERR_OK)
    {
        res.append(0);
    }
    else
    {
        if (ansver.length() % 2 != 0)
        {
            res.append(0);
        }
        else
        {
            ByteArray2short t;
            for (int i = 0; i < ansver.length(); i += 2)
            {
                memcpy(t.raw, ansver.data() + i, 2);
                res.append(t.result);
            }
        }
    }
    return res;
}

void ModbusRequest::EmitSignal()
{
    emit updated();
}

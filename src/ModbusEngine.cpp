/*
 * ModbusEngine.cpp
 *
 *  Created on: 06.04.2012
 *      Author: tolyan
 */

#include <QByteArray>
#include <QMutex>
#include <QTimer>
#include <modbus/modbus.h>
#include <errno.h>

#include <mylog/mylog.h>
#include <settingscmdline/settingscmdline.h>

#include "common.h"
#include "libmodbusRAWWarper.h"
#include "PerfTimeCounter.h"
#include "ModbusRequest.h"

#include "ModbusEngine.h"

#ifdef NDEBUG
#undef LOG_ERROR
#define LOG_ERROR(msg)
#undef LOG_WARNING
#define LOG_WARNING(msg)
#undef LOG_INFO
#define LOG_INFO(msg)
#undef LOG_DEBUG
#define LOG_DEBUG(msg)
#undef LOG_type
#define LOG_type(msg, type)
#endif

#ifdef  WIN32
#define __DEFAULT__COM__PORT__      "COM1"
#else
#define __DEFAULT__COM__PORT__      "/dev/ttyS0"
#endif
#define __DEFAULT__BOUD__           (115200)

unsigned char ModbusEngine::currentNormalAnsverLen;

ModbusEngine::ModbusEngine()
{
    AutoupdateGlobalEnabled = false;
    ProcessTerminateMutex.lock();
    AdditionalUpdateSycleDelay = 1;
    mb = NULL;
    setTryCloseonError(true);
    RestartConnection();
}

ModbusEngine::~ModbusEngine()
{
    ProcessTerminateMutex.unlock(); // сигнал разрешающий процессу выйти из бесконечного цыкла
    msleep(getCycleDelay() + 10); // джем, что мьютекс будет схвачен в процессе выхода из потока
    // если удачно схватили, то поток не запущен совсем
    // если неудачно схватили, то поток завершился.
    this->exit(0);
    __DELETE(mb);
}

void ModbusEngine::AddAutocallRequest(ModbusRequest* request)
{
    AutocallRequests.append(request);
    request->setParent(this);
}

bool ModbusEngine::RemoveAutocallRequest(ModbusRequest* request)
{
    if (AutocallRequests.removeOne(request))
    {
        request->setParent(NULL);
        return true;
    }
    else
        return false;
}

int ModbusEngine::SyncRequest(ModbusRequest& request)
{
    /*
     * Эта функция может быть вызвана напрямую, из GUI потока, или из автоапдейтера
     */
    if (!request.checkEnabled())
        return 0;
    int result;
    //PerfTimeCounter perfcounter;
    //unsigned long t1, t2, t3, t4;
    unsigned char Address = request.getDeviceAddress();
    QByteArray reqData = request.getRequest();
    QByteArray ansData;
    enum ModbusRequest::ErrorCode errorr;
    OperationInProgressMutex.lock();
    if (mb == NULL)
    {
        // модбас не существует, ошибка
        request.setErrorCode(ModbusRequest::ERR_NOT_OPENED);
        result = -3;
    }
    else
    {
        int connectRes = mb->connect(Address);
        switch (connectRes)
            //попытка преключиться на новый адресс
        {
        case -2:
        { // невозможно открыть порт, ошибка вида 'ERROR Can't open the device COMx (Нет такого файла или каталога)'
            emit ModbusGeneralFailure(2);
            result = 2;
            request.setErrorCode(ModbusRequest::ERR_NOT_OPENED);
        }
            break;
        case -1:
        { // ModBus не создано
            emit ModbusGeneralFailure(1);
            result = 1;
            request.setErrorCode(ModbusRequest::ERR_NOT_OPENED);
        }
            break;
        default:
        {
            /*
         * выполняем запрос
         */
            //perfcounter.Start();
            //mb->flush();
            if (request.getRequestType()
                    == ModbusRequest::MB_REQUEST_TYPE_RAWREQUEST_WITH_HOCK)
            {
                mb->SetUseCRC16(false); //отключить СRC
                ModbusRequest::_pfHoockFun hoock = request.getpfHoockFun();
                if (hoock == NULL)
                { // если особый хук не установлен, ьерем дефаултный
                    hoock = defaultHoockfun;
                    currentNormalAnsverLen = request.getNormalAnsverLen();
                }
                mb->SetHoock(hoock); // установить хук
                //t1 = perfcounter.Catch();
                unsigned char tAbuf[254];
                memcpy(tAbuf, reqData.constData(), reqData.size());
                //mb->connect(Address); // долго делается
                //t2 = perfcounter.Catch();
                mb->SendRAWData(tAbuf, reqData.size());
                msleep(2);
                signed char ansveLen = mb->ReceiveRAWData(tAbuf);
                //t3 = perfcounter.Catch();
                //mb->close(); // долго делается
                //t4 = perfcounter.Catch();
                //t1 = t1 + t2 + t3 + t4;

                if (ansveLen >= 2)
                {
                    if (tAbuf[1] & (1 << 7))
                    {
                        errorr = (enum ModbusRequest::ErrorCode) tAbuf[2];
                    }
                    else
                    {
                        ansData = QByteArray((char*) (tAbuf + 1), ansveLen - 1);
                        errorr = ModbusRequest::ERR_OK;
                    }
                }
                else
                {
                    errorr = ModbusRequest::ERR_UNKNOWN;
                }
            }
            else
            {
                mb->SetUseCRC16(true);
                mb->SetHoock((ModbusRequest::_pfHoockFun) NULL);

                switch (request.getRequestType())
                {
                case ModbusRequest::MB_REQUEST_TYPE_RAWREQUEST:
                    break;
                case ModbusRequest::MB_REQUEST_TYPE_READ_SINGLE_COIL:
                    break;
                case ModbusRequest::MB_REQUEST_TYPE_READ_MULTI_COIL:
                {
                    uint8_t ansver[256];
                    union ByteArray2Twoint16 convertTemp;
                    memcpy(convertTemp.raw, reqData.data(), 4);
                    int length = modbus_read_bits(
                                mb->getModbusContext(), convertTemp.uint[0],
                            convertTemp.uint[1], ansver);
                    if (length < 1)
                    { //ошибка
                        errorr = (ModbusRequest::ErrorCode) (
                                    (errno - MODBUS_ENOBASE <= 12) ?
                                        (errno - MODBUS_ENOBASE) : (errno));
                    }
                    else
                    { //ок
                        // библиотека возвращает ответ побайтно, конвертируем назад
                        // в битовое поле
                        ansver[0] = (ansver[0]) ? (1) : (0); // гарантировано

                        for (unsigned char i = 1; i < length; ++i)
                            ansver[i / 8] |= (ansver[i]) ? (1 << (i % 8)) : 0;
                        ansData = QByteArray((char*) ansver,
                                             ((length / 8) + 1) * sizeof(uint8_t));
                        errorr = ModbusRequest::ERR_OK;
                    }
                }
                    break;
                case ModbusRequest::MB_REQUEST_TYPE_WRITE_SINGLE_COIL:
                {
                    union ByteArray2Twoint16 convertTemp;
                    memcpy(convertTemp.raw, reqData.data(), 4);
                    int length = modbus_write_bit(mb->getModbusContext(),
                                                  convertTemp.uint[0], convertTemp.uint[1]);
                    if (length < 1)
                    { //ошибка
                        errorr = (ModbusRequest::ErrorCode) (
                                    (errno - MODBUS_ENOBASE <= 12) ?
                                        (errno - MODBUS_ENOBASE) : (errno));
                    }
                    else
                    { //ок
                        ansData = QByteArray(1, 1);
                        errorr = ModbusRequest::ERR_OK;
                    }
                }
                    break;
                case ModbusRequest::MB_REQUEST_TYPE_WRITE_MULTI_COIL:
                {
                    uint16_t startCoil;
                    uint16_t Len;
                    memcpy((char*) &startCoil, reqData.data(), 2); //стартовый адрес
                    memcpy((char*) &Len, reqData.data() + 2, 2); //длина
                    unsigned char dataBytes[Len];
                    for (int i = 0; i < Len; ++i)
                        dataBytes[i] = ((unsigned char*)(reqData.data() + 4))[i / 8] & (1 << (i % 8)) ? TRUE : FALSE;
                    int length = modbus_write_bits(mb->getModbusContext(),
                                                   startCoil,
                                                   Len,
                                                   dataBytes);
                    if (length == Len)
                    {
                        ansData = QByteArray(1, length);
                        errorr = ModbusRequest::ERR_OK;
                    }
                    else
                    {
                        errorr = (ModbusRequest::ErrorCode) (
                                    (errno - MODBUS_ENOBASE <= 12) ?
                                        (errno - MODBUS_ENOBASE) : (errno));
                    }
                }
                    break;
                case ModbusRequest::MB_REQUEST_TYPE_READ_HOLDING_REGISTERS:
                {
                    uint16_t ansver[128];
                    union ByteArray2Twoint16 convertTemp;
                    memcpy(convertTemp.raw, reqData.data(), 4);
                    int length = modbus_read_registers(mb->getModbusContext(),
                                                       convertTemp.uint[0], convertTemp.uint[1], ansver);
                    if (length < 1)
                    { //ошибка
                        errorr = (ModbusRequest::ErrorCode) (
                                    (errno - MODBUS_ENOBASE <= 12) ?
                                        (errno - MODBUS_ENOBASE) : (errno));
                    }
                    else
                    { //ок
                        ansData = QByteArray((char*) ansver,
                                             length * sizeof(uint16_t));
                        errorr = ModbusRequest::ERR_OK;
                    }
                }
                    break;
                case ModbusRequest::MB_REQUEST_TYPE_WRITE_HOLDING_REGISTERS:
                {
                    uint16_t startAddress;
                    uint16_t Len;
                    memcpy((char*) &startAddress, reqData.data(), 2); //стартовый адрес
                    memcpy((char*) &Len, reqData.data() + 2, 2); //длина
                    int length = modbus_write_registers(mb->getModbusContext(),
                                                        startAddress, Len,
                                                        (uint16_t*) (reqData.data() + 4));
                    if (length == Len)
                    {
                        ansData = QByteArray(1, length);
                        errorr = ModbusRequest::ERR_OK;
                    }
                    else
                    {
                        errorr = (ModbusRequest::ErrorCode) (
                                    (errno - MODBUS_ENOBASE <= 12) ?
                                        (errno - MODBUS_ENOBASE) : (errno));
                    }
                }
                    break;
                case ModbusRequest::MB_REQUEST_TYPE_READ_INPUTS:
                {
                    uint16_t ansver[128];
                    union ByteArray2Twoint16 convertTemp;
                    memcpy(convertTemp.raw, reqData.data(), 4);
                    int length = modbus_read_input_registers(
                                mb->getModbusContext(), convertTemp.uint[0],
                            convertTemp.uint[1], ansver);
                    if (length < 1)
                    { //ошибка
                        errorr = (ModbusRequest::ErrorCode) (
                                    (errno - MODBUS_ENOBASE <= 12) ?
                                        (errno - MODBUS_ENOBASE) : (errno));
                    }
                    else
                    { //ок
                        ansData = QByteArray((char*) ansver,
                                             length * sizeof(uint16_t));
                        errorr = ModbusRequest::ERR_OK;
                    }
                }
                    break;
                case ModbusRequest::MB_REQUEST_TYPE_READ_DISCRET_INPUTS:
                {
                    uint8_t ansver[256];
                    union ByteArray2Twoint16 convertTemp;
                    memcpy(convertTemp.raw, reqData.data(), 4);
                    int length = modbus_read_input_bits(
                                mb->getModbusContext(), convertTemp.uint[0],
                            convertTemp.uint[1], ansver);
                    if (length < 1)
                    { //ошибка
                        errorr = (ModbusRequest::ErrorCode) (
                                    (errno - MODBUS_ENOBASE <= 12) ?
                                        (errno - MODBUS_ENOBASE) : (errno));
                    }
                    else
                    { //ок
                        // библиотека возвращает ответ побайтно, конвертируем назад
                        // в битовое поле
                        ansver[0] = (ansver[0]) ? (1) : (0); // гарантировано

                        for (unsigned char i = 1; i < length; ++i)
                            ansver[i / 8] |= (ansver[i]) ? (1 << (i % 8)) : 0;
                        ansData = QByteArray((char*) ansver,
                                             ((length / 8) + 1) * sizeof(uint8_t));
                        errorr = ModbusRequest::ERR_OK;
                    }
                }
                    break;
                case ModbusRequest::MB_REQUEST_TYPE_WRITE_HOLDING_REGISTER:
                {
                    uint16_t address;
                    memcpy((char*) &address, reqData.data(), 2); //стартовый адрес
                    int length = modbus_write_register(mb->getModbusContext(),
                                                       address,
                                                       *(uint16_t*) (reqData.data() + 2));
                    if (length == 1)
                    {
                        ansData = QByteArray(1, length);
                        errorr = ModbusRequest::ERR_OK;
                    }
                    else
                    {
                        errorr = (ModbusRequest::ErrorCode) (
                                    (errno - MODBUS_ENOBASE <= 12) ?
                                        (errno - MODBUS_ENOBASE) : (errno));
                    }
                }
                    break;
                case ModbusRequest::MB_REQUEST_TYPE_EMPTY:
                    break;
                }

            }
            request.setAnsver(ansData);
            request.setErrorCode(errorr);
            if (errorr != ModbusRequest::ERR_OK)
            {
                LOG_ERROR(
                            trUtf8( "Modbus request to %1 failed, errorcode=%2 (req. type=%3)").arg( Address).arg(errorr).arg( (uint) request.getRequestType()));
                if (tryCloseonError)
                    mb->close(); // IMPORTANT: не дает ломаться интерефейсу при возникновении первой ошибки.
            }
            result = 0;
        }
            break;
        }
    }
    OperationInProgressMutex.unlock();
    if (result != 0)
    {
        LOG_ERROR(trUtf8("Modbus connect error detected (%1)").arg(result));
    }
    return result;
}

void ModbusEngine::SetCycleDelay(unsigned long NewDelay)
{
    UpdateDelayMutex.lock();
    AdditionalUpdateSycleDelay = NewDelay;
    UpdateDelayMutex.unlock();
    LOG_INFO(QObject::trUtf8("Autoupdate interval set to %1").arg(NewDelay));
}

unsigned long ModbusEngine::getCycleDelay(void)
{
    unsigned long res;
    UpdateDelayMutex.lock();
    res = AdditionalUpdateSycleDelay;
    UpdateDelayMutex.unlock();
    return res;
}

void ModbusEngine::AddListOfAutocallRequests(QList<ModbusRequest*> &requestList)
{
    QList<ModbusRequest*>::iterator it = requestList.begin();
    for (; it < requestList.end(); ++it)
        AddAutocallRequest(*it);
}

int ModbusEngine::RemoveListOfAutocallRequests(
        QList<ModbusRequest*> &requestList)
{
    int res = 0;
    QList<ModbusRequest*>::iterator it = requestList.begin();
    for (; it < requestList.end(); ++it)
    {
        if (AutocallRequests.removeOne(*it))
        {
            (*it)->setParent(NULL);
            ++res;
        }
    }
    return res;
}

void ModbusEngine::run(void)
{
    SetCycleDelay(1);
    while (!ProcessTerminateMutex.tryLock())
    {
        if (isAutoupdateGlobalEnabled())
        {
            if (!AutocallRequests.isEmpty())
            {
                bool isOk = true;
                QList<ModbusRequest*>::iterator it = AutocallRequests.begin(); //итератор
                for (; it < AutocallRequests.end(); ++it)
                {
#ifdef INF_AUTOUPDATE_TRY
                    while (true)
                    {
                        if (SyncRequest(**it) != 0)
                        { //!0 при совсем фэйле, останавливаемся
#ifndef _NODEVICES
                            isOk = false;
#endif
                            break;
                        }
                        else
                        { // проверим код ошибки
                            if ((*it)->getErrorrCode() != ModbusRequest::ERR_OK)
                                msleep(50);//вслучае её возникновения ждем и пробуем заново
                            else
                            {
                                (*it)->EmitSignal();
                                break; //все ок, продолжаем.
                            }
                        }
                    }
#else
                    if (SyncRequest(**it) != 0)
                    { //!0 при совсем фэйле, останавливаемся
#ifndef _NODEVICES
                        isOk = false;
#endif
                        break;
                    }
#endif
                    (*it)->EmitSignal();
                }
                if (isOk)
                {
                    emit
                    AutoupdateComplead();
                }
            }
        }
        msleep(getCycleDelay());
    }
}

void ModbusEngine::RestartConnection(void)
{
    LOG_WARNING(trUtf8("ModbusEngine::RestartConnection"));
    OperationInProgressMutex.lock(); //мьютекс не аст таймеру выполнить действие на несуществующем modbu'се
    if (mb != NULL)
        __DELETE(mb);
    QString portName = SettingsCmdLine::settings->value("Global/Port", __DEFAULT__COM__PORT__).toString();
    LOG_INFO(trUtf8("Opening serial device \"%1\"").arg(portName));
    mb =
            new libmodbusRAWWarper(
                portName.toLatin1(),
                SettingsCmdLine::settings->value("Global/Baud", __DEFAULT__BOUD__).toUInt(),
                'N', 8, 1);
    if (mb->getModbusContext() == NULL)
    {
        LOG_FATAL(trUtf8("Opening failed"));
    }
    OperationInProgressMutex.unlock();
}

void ModbusEngine::setTimeout(unsigned int timeout_ms)
{
#if LIBMODBUS_VERSION_HEX < 0x030101
    struct timeval timeout = {0, timeout_ms * 1000};
    modbus_set_response_timeout(mb->getModbusContext(), &timeout);
#else
    modbus_set_response_timeout(mb->getModbusContext(), 0, timeout_ms * 1000);
#endif
}

void ModbusEngine::close()
{
    mb->close();
}

bool ModbusEngine::isAutoupdateGlobalEnabled() const
{
    bool res;
    UpdateEnableMutex.lock();
    res = AutoupdateGlobalEnabled;
    UpdateEnableMutex.unlock();
    return res;
}

void ModbusEngine::setAutoupdateGlobalEnabled(bool autoupdateGlobalEnabled)
{
    UpdateEnableMutex.lock();
    LOG_DEBUG(
                trUtf8("%1 autoupdate cycle").arg( autoupdateGlobalEnabled ? trUtf8("Enabling") : trUtf8("Disabling")));
    AutoupdateGlobalEnabled = autoupdateGlobalEnabled;
    UpdateEnableMutex.unlock();
}

unsigned char ModbusEngine::defaultHoockfun(unsigned char* buff,
                                            char resiverlen)
{
    return currentNormalAnsverLen;
}

modbus_t* ModbusEngine::getModbusContext() const
{
    return mb->getModbusContext();
}

void ModbusEngine::setTryCloseonError(bool v)
{
    OperationInProgressMutex.lock();
    tryCloseonError = v;
    OperationInProgressMutex.unlock();
}
